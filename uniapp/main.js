import Vue from 'vue'
import App from './App'
Vue.prototype.webtitle = "易风课堂"
Vue.prototype.weburl ="http://www.yifengkt.cn"
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
