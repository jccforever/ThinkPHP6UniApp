<?php /*a:1:{s:103:"F:\2019年视频课程\第二季 ThinkPHP6.0+uniapp项目实战\code\yifeng\view\admin\classs\edit.html";i:1563505063;}*/ ?>
<!DOCTYPE html>
<html class="x-admin-sm">
    
    <head>
        <meta charset="UTF-8">
        <title>编辑配置类型</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/static/admin/css/font.css">
        <link rel="stylesheet" href="/static/admin/css/xadmin.css">
        <script type="text/javascript" src="/static/admin/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/static/admin/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
            <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
            <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row">
                <form class="layui-form">
                  <div class="layui-form-item">
                      <label for="name" class="layui-form-label">
                          <span class="x-red">*</span>名称
                      </label>
                      <div class="layui-input-inline">
                          <input type="text" id="name" value="<?php echo htmlentities($classs['name']); ?>" name="name" required=""
                          autocomplete="off" class="layui-input">
                      </div>
                  </div>
                  <div class="layui-form-item">
                      <label for="sort" class="layui-form-label">
                          <span class="x-red">*</span>排序
                      </label>
                      <div class="layui-input-inline">
                          <input type="text" id="sort" name="sort" value="<?php echo htmlentities($classs['sort']); ?>" required=""
                          autocomplete="off" class="layui-input">
                      </div>
                  </div>

                  <div class="layui-form-item">
                      <label for="add" class="layui-form-label">
                      </label>
                      <button  class="layui-btn" id="add" lay-filter="add" lay-submit="">
                          保存
                      </button>
                  </div>
              </form>
            </div>
        </div>
        <script>layui.use(['form', 'layer'],
            function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;
                //监听提交
                form.on('submit(add)',
                function(data) {
                    console.log(data.field);
                    //发异步，把数据提交给php
                    $.post("/admin/classs/update/<?php echo htmlentities($classs['id']); ?>",data.field,function(res){
                        layer.alert(res.msg, {
                                icon: 6
                            },
                            function(index) {
                               if(res.code==1){
                                   //关闭当前frame
                                   xadmin.close();
                                   // 可以对父窗口进行刷新
                                   xadmin.father_reload();
                               }
                                layer.close(index);
                            });
                    });
                    return false;
                });

            });</script>
    </body>

</html>
