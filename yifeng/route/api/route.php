<?php
/**
 * * * * * * * * * * * * * * * * * * * * * *
 * 易风课堂系列课程                     　    *
 * * * * * * * * * * * * * * * * * * * * * *
 * password:yifeng                             *
 * * * * * * * * * * * * * * * * * * * * * *
 * www.yifengkt.cn                         *
 * * * * * * * * * * * * * * * * * * * * * *
 * QQ:576617109                            *
 * * * * * * * * * * * * * * * * * * * * * *
 */

use think\facade\Route;

// Route::get("/:ver/test", ":ver.Index/test");
//短信接口
Route::post("/:ver/getcode", ":ver.Index/getsmscode");
//密码找回接口
Route::post("/:ver/users/findpassword", ":ver.Users/findpassword");
//用户注册
Route::post("/:ver/users", ":ver.Users/create");

//基础参数
Route::post('/:ver/getconfig',":ver.Classs/getClasss")->middleware('check');
//用户登录
Route::post("/:ver/login", ":ver.Login/login")->allowCrossDomain(['Access-Control-Allow-Headers'=>'sign,content-type']);
Route::post('/:ver/test1',":ver.Login/test1")->middleware('check');
// Route::post('/:ver/test',":ver.Login/test")->middleware('check');
Route::post('/:ver/test',":ver.Login/test");
Route::get("/:ver", ":ver.Index/index");