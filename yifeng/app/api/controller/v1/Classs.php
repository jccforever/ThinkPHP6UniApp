<?php
declare (strict_types = 1);

namespace app\api\controller\v1;
use \app\admin\model\Classs as ClasssModel;
use app\api\libs\Jwtauth;
use app\api\model\User;
use think\Request;

class Classs extends Common
{
    public function getClasss(){
        $config = ClasssModel::select();
        $data = $this->formatData($config);
        return json($data);
    }

    /**
     * 获会员的级别
     * @return mixed
     */
    private function getlevel(){
        $username = Jwtauth::getInstance()->getUsername();
        $level = User::where('username','=',$username)->value("level");
        return $level;
    }

    /**
     * 格式化数据
     * @param $config
     * @return array
     */
    private function formatData($config){
        $data = [];
        foreach ($config as $item){
            $arr = [];
            // $arr[$item->label] =$item->classitem->hidden(["id","formtype","sort","classid","nvalue"]);
            foreach ($item->classitem as $rs){
                $tmp = [];
                if($item->label == "fumo"){
                    switch ($rs->name){
                        case "哑膜":
                            $arr['ya']=$rs->value;
                            break;
                        case "亮膜":
                            $arr['liang']=$rs->value;
                            break;
                        case "不覆膜":
                            $arr['wu']=$rs->value;
                            break;
                    }
                }else{
                    $tmp['name']=$rs->name;
                    $tmp['price']=$rs->value;
                    if($item->label == "yinshua") $tmp['zhangfu']=$rs->zhangfu;
                    if($this->getlevel()){
                        $tmp['price']=$rs->nvalue;
                        if($item->label == "yinshua") $tmp['zhangfu']=$rs->nzhangfu;
                    }
                    $arr[] = $tmp;
                }

            }
            $data[$item->label] = $arr;
        }
        return $data;
    }
}
